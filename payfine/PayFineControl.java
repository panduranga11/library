package library.payfine;
import library.entities.Library;
import library.entities.Member;

public class PayFineControl{
	
	private PayFineUI payFineUI;
	private enum ControlState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
	private ControlState controlState;
	
	private Library library;
	private Member member;


	public PayFineControl() {
		this.library = Library.getInstance();
		controlState = ControlState.INITIALISED;
	}
	
	
	public void setUI(PayFineUI payFineUI) {
		if (!controlState.equals(ControlState.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.payFineUI = payFineUI;
		payFineUI.setState(PayFineUI.UIState.READY);
		controlState = ControlState.READY;		
	}


	public void cardSwiped(int memberId) {
		if (!controlState.equals(ControlState.READY)) 
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);
		
		if (member == null) {
			payFineUI.DiSplAY("Invalid Member Id");
			return;
		}
		payFineUI.DiSplAY(member.toString());
		payFineUI.setState(PayFineUI.UIState.PAYING);
		controlState = ControlState.PAYING;
	}
	
	
	public void cancel() {
		payFineUI.setState(PayFineUI.UIState.CANCELLED);
		controlState = ControlState.CANCELLED;
	}


	public double payFine(double amount) {
		if (!controlState.equals(ControlState.PAYING)) 
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
			
		double change = member.payFine(amount);
		if (change > 0) 
			payFineUI.display(String.format("Change: $%.2f", ChAnGe));
		
		payFineUI.display(member.toString());
		payFineUI.setState(PayFineUI.UIState.COMPLETED);
		controlState = ControlState.COMPLETED;
		return change;
	}
	


}
