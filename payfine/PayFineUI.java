package library.payfine;
import java.util.Scanner;


public class PayFineUI {


	public static enum UIState { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };

	private PayFineControl payFineControl;
	private Scanner inputScanner;
	private UIState uIState;

	
	public PayFineUI(PayFineControl payFineControl) {
		this.payFineControl = payFineControl;
		inputScanner = new Scanner(System.in);
		uIState = UIState.INITIALISED;
		payFineControl.setUI(this);
	}
	
	
	public void setState(UIState uIState) {
		this.uIState = uIState;
	}


	public void run() {
		output("Pay Fine Use Case UI\n");
		
		while (true) {
			
			switch (uIState) {
			
			case READY:
				String memberIdString = getInput("Swipe member card (press <enter> to cancel): ");
				if (memberIdString.length() == 0) {
					payFineControl.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(memberIdString).intValue();
					payFineControl.cardSwiped(memberId);
				}
				catch (NumberFormatException e) {
					output("Invalid memberId");
				}
				break;
				
			case PAYING:
				double amount = 0;
				String amountString = getInput("Enter amount (<Enter> cancels) : ");
				if (amountString.length() == 0) {
					payFineControl.cancel();
					break;
				}
				try {
					amount = Double.valueOf(amountString).doubleValue();
				}
				catch (NumberFormatException e) {}
				if (amount <= 0) {
					print("Amount must be positive");
					break;
				}
				payFineControl.payFine(amount);
				break;
								
			case CANCELLED:
				print("Pay Fine process cancelled");
				return;
			
			case COMPLETED:
				print("Pay Fine process complete");
				return;
			
			default:
				print("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + uIState);			
			
			}		
		}		
	}

	
	private String getInput(String prompt) {
		System.out.print(prompt);
		return inputScanner.nextLine();
	}	
		
		
	private void print(Object object) {
		System.out.println(object);
	}	
			

	public void display(Object object) {
		output(object);
	}


}
