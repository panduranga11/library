package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

	private ReturnBookUI returnBookUI;
	private enum ControlState { INITIALISED, READY, INSPECTING };
	private ControlState controlState;
	
	private Library library;
	private Loan currentLoan;
	

	public ReturnBookControl() {
		this.library = Library.getInstance();
		controlState = controlState.INITIALISED;
	}
	
	
	public void setUI(ReturnBookUI returnBookUI) {
		if (!controlState.equals(controlState.INITIALISED)) 
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
		
		this.returnBookUI = returnBookUI;
		returnBookUI.setState(ReturnBookUI.UIState.READY);
		controlState = controlState.READY;		
	}


	public void bookScanned(int bookId) {
		if (!controlState.equals(controlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
		
		Book currentBook = library.getBookById(bookId);
		
		if (currentBook == null) {
			returnBookUI.display("Invalid Book Id");
			return;
		}
		if (!currentBook.isOnLoan()) {
			returnBookUI.display("Book has not been borrowed");
			return;
		}		
		currentLoan = library.getLoanByBookId(bookId);	
		double overDueFine = 0.0;
		if (currentLoan.isOverDue()) 
			overDueFine = library.calculateOverdueFine(currentLoan);
		
		returnBookUI.display("Inspecting");
		returnBookUI.display(currentBook.toString());
		returnBookUI.display(currentLoan.toString());
		
		if (currentLoan.isOverDue()) 
			returnBookUI.display(String.format("\nOverdue fine : $%.2f", overDueFine));
		
		returnBookUI.setState(ReturnBookUI.UIState.INSPECTING);
		controlState = controlState.INSPECTING;		
	}


	public void scanningComplete() {
		if (!controlState.equals(controlState.READY)) 
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
			
		returnBookUI.setState(ReturnBookUI.UIState.COMPLETED);		
	}


	public void dischargeLoan(boolean isDamanged) {
		if (!controlState.equals(controlState.INSPECTING)) 
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
		
		library.dischargeLoan(currentLoan, isDamanged);
		currentLoan = null;
		returnBookUI.setState(ReturnBookUI.UIState.READY);
		controlState = controlState.READY;				
	}


}
