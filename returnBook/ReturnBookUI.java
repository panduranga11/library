package library.returnBook;
import java.util.Scanner;


public class ReturnBookUI {

	public static enum UIState { INITIALISED, READY, INSPECTING, COMPLETED };

	private ReturnBookControl returnBookControl;
	private Scanner inputScanner;
	private UIState uIState;

	
	public ReturnBookUI(ReturnBookControl returnBookControl) {
		this.returnBookControl = returnBookControl;
		inputScanner = new Scanner(System.in);
		uIState = UIState.INITIALISED;
		returnBookControl.setUI(this);
	}


	public void run() {		
		oUtPuT("Return Book Use Case UI\n");
		
		while (true) {
			
			switch (uIState) {
			
			case INITIALISED:
				break;
				
			case READY:
				String bookIdString = getInput("Scan Book (<enter> completes): ");
				if (bookIdString.length() == 0) 
					returnBookControl.scanningComplete();				
				else {
					try {
						int bookId = Integer.valueOf(bookIdString).intValue();
						returnBookControl.bookScanned(bookId);
					}
					catch (NumberFormatException e) {
						print("Invalid bookId");
					}					
				}
				break;				
				
			case INSPECTING:
				String answer = getInput("Is book damaged? (Y/N): ");
				boolean isDamanged = false;
				if (answer.toUpperCase().equals("Y")) 					
					isDamanged = true;
				
				returnBookControl.dischargeLoan(isDamanged);
			
			case COMPLETED:
				print("Return processing complete");
				return;
			
			default:
				print("Unhandled state");
				throw new RuntimeException("ReturnBookUI : unhandled state :" + uIState);			
			}
		}
	}

	
	private String getInput(String prompt) {
		System.out.print(prompt);
		return inputScanner.nextLine();
	}			
		
	private void print(Object object) {
		System.out.println(object);
	}	
			
	public void setState(UIState uIState) {
		this.uIState = uIState;
	}

	public void display(Object object) {
		print(object);		
	}

	
}
