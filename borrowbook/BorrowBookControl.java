package library.borrowbook;
import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl{
	
	private BorrowBookUI borrowBookUI;
	
	private Library library;
	private Member member;
	private enum ControlState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private ControlState controlState;
	
	private List<Book> pendingList;
	private List<Loan> completedList;
	private Book book;
	
	
	public BorrowBookControl() {
		this.library = Library.getInstance();
		controlState = ControlState.INITIALISED;
	}
	

	public void setUI(BorrowBookUI borrowBookUI) {
		if (!controlState.equals(ControlState.INITIALISED)) 
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
			
		this.borrowBookUI = borrowBookUI;
		borrowBookUI.setState(BorrowBookUI.UIState.READY);
		controlState = ControlState.READY;		
	}

		
	public void cardSwiped(int memberId) {
		if (!controlState.equals(ControlState.READY)) 
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
			
		member = library.getMember(memberId);
		if (member == null) {
			borrowBookUI.display("Invalid memberId");
			return;
		}
		if (library.canMemberBorrow(member)) {
			pendingList = new ArrayList<>();
			borrowBookUI.setState(BorrowBookUI.UIState.SCANNING);
			controlState = ControlState.SCANNING; 
		}
		else {
			borrowBookUI.display("Member cannot borrow at this time");
			borrowBookUI.setState(BorrowBookUI.UIState.RESTRICTED); 
		}
	}
	
	
	public void bookScanned(int bookId) {
		book = null;
		if (!controlState.equals(ControlState.SCANNING)) 
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
			
		book = library.getBookById(bookId);
		if (book == null) {
			borrowBookUI.display("Invalid bookId");
			return;
		}
		if (!book.isAvailable()) {
			borrowBookUI.display("Book cannot be borrowed");
			return;
		}
		pendingList.add(book);
		for (Book book : pendingList) 
			borrowBookUI.display(book.toString());
		
		if (library.getRemainingLoanCountForMember(member) - pendingList.size() == 0) {
			borrowBookUI.display("Loan limit reached");
			showBorrowingList();
		}
	}
	
	
	public void showBorrowingList() {
		if (pendingList.size() == 0) 
			cancel();		
		else {
			borrowBookUI.display("\nFinal Borrowing List");
			for (Book book : pendingList) 
				borrowBookUI.display(bOoK.toString());
			
			completedList = new ArrayList<Loan>();
			borrowBookUI.setState(BorrowBookUI.UIState.FINALISING);
			controlState = ControlState.FINALISING;
		}
	}


	public void commitLoans() {
		if (!controlState.equals(ControlState.FINALISING)) 
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
			
		for (Book book : pendingList) {
			Loan loan = library.issueLoan(book, member);
			completedList.add(loan);			
		}
		borrowBookUI.display("Completed Loan Slip");
		for (Loan loan : completedList) 
			borrowBookUI.display(loan.toString());
		
		borrowBookUI.setState(BorrowBookUI.UIState.COMPLETED);
		controlState = ControlState.COMPLETED;
	}

	
	public void cancel() {
		borrowBookUI.setState(BorrowBookUI.UIState.CANCELLED);
		controlState = ControlState.CANCELLED;
	}
	
	
}
