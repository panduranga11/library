package library.borrowbook;
import java.util.Scanner;


public class BorrowBookUI {
	
	public static enum UIState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };

	private BorrowBookControl borrowBookControl;
	private Scanner inputScanner;
	private UIState uIState;
	
	public BorrowBookUI(BorrowBookControl borrowBookControl) {
		this.borrowBookControl = borrowBookControl;
		inputScanner = new Scanner(System.in);
		uIState = UIState.INITIALISED;
		control.setUI(this);
	}
	
	private String getInput(String prompt) {
		System.out.print(prompt);
		return inputScanner.nextLine();
	}			
		
	private void print(Object object) {
		System.out.println(object);
	}	
			
	public void setState(UIState uIState) {
		this.uIState = uIState;
	}
	
	public void run() {
		print("Borrow Book Use Case UI\n");
		
		while (true) {
			
			switch (uIState) {			
			
			case CANCELLED:
				print("Borrowing Cancelled");
				return;
				
			case READY:
				String memberCard = getInput("Swipe member card (press <enter> to cancel): ");
				if (memberCard.length() == 0) {
					borrowBookControl.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(memberCard).intValue();
					borrowBookControl.cardSwiped(memberId);
				}
				catch (NumberFormatException e) {
					print("Invalid Member Id");
				}
				break;
				
			case RESTRICTED:
				getInput("Press <any key> to cancel");
				borrowBookControl.cancel();
				break;			
				
			case SCANNING:
				String bookIdString = getInput("Scan Book (<enter> completes): ");
				if (bookIdString.length() == 0) {
					borrowBookControl.showBorrowingList();
					break;
				}
				try {
					int bookId = Integer.valueOf(bookIdString).intValue();
					borrowBookControl.bookScanned(bookId);
					
				} catch (NumberFormatException e) {
					print("Invalid Book Id");
				} 
				break;					
				
			case FINALISING:
				String answer = getInput("Commit loans? (Y/N): ");
				if (answer.toUpperCase().equals("N")) {
					borrowBookControl.cancel();					
				} else {
					borrowBookControl.commitLoans();
					getInput("Press <any key> to complete ");
				}
				break;
								
			case COMPLETED:
				print("Borrowing Completed");
				return;
				
			default:
				print("Unhandled state");
				throw new RuntimeException("BorrowBookUI : unhandled state :" + uIState);			
			}
		}		
	}


	public void display(Object object) {
		print(object);		
	}


}
