package library;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import library.borrowbook.BorrowBookUI;
import library.borrowbook.BorrowBookControl;
import library.entities.Book;
import library.entities.Calendar;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;
import library.fixbook.FixBookUI;
import library.fixbook.FixBookControl;
import library.payfine.PayFineUI;
import library.payfine.PayFineControl;
import library.returnBook.ReturnBookUI;
import library.returnBook.ReturnBookControl;


public class Main {
	
	private static Scanner inputScanner;
	private static Library library;
	private static String menu;
	private static Calendar calendar;
	private static SimpleDateFormat dateFormatter;
	
	
	private static String getMenu() {
		StringBuilder menuBuilder = new StringBuilder();
		
		menuBuilder.append("\nLibrary Main Menu\n\n")
		  .append("  M  : add member\n")
		  .append("  LM : list members\n")
		  .append("\n")
		  .append("  B  : add book\n")
		  .append("  LB : list books\n")
		  .append("  FB : fix books\n")
		  .append("\n")
		  .append("  L  : take out a loan\n")
		  .append("  R  : return a loan\n")
		  .append("  LL : list loans\n")
		  .append("\n")
		  .append("  P  : pay fine\n")
		  .append("\n")
		  .append("  T  : increment date\n")
		  .append("  Q  : quit\n")
		  .append("\n")
		  .append("Choice : ");
		  
		return menuBuilder.toString();
	}


	public static void main(String[] args) {		
		try {			
			inputScanner = new Scanner(System.in);
			library = Library.getInstance();
			calendar = Calendar.getInstance();
			dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
	
			for (Member member : library.getMembers()) {
				print(member);
			}
			print(" ");
			for (Book book : library.getBooks()) {
				print(book);
			}
						
			menu = getMenu();
			
			boolean exit = false;
			
			while (!exit) {
				
				print("\n" + dateFormatter.format(calendar.getDate()));
				String menuItem = getInput(menu);
				
				switch (menuItem.toUpperCase()) {
				
				case "M": 
					addMember();
					break;
					
				case "LM": 
					listMembers();
					break;
					
				case "B": 
					addBook();
					break;
					
				case "LB": 
					listBooks();
					break;
					
				case "FB": 
					fixBooks();
					break;
					
				case "L": 
					borrowBook();
					break;
					
				case "R": 
					returnBook();
					break;
					
				case "LL": 
					listCurrentLoans();
					break;
					
				case "P": 
					payFines();
					break;
					
				case "T": 
					incrementDate();
					break;
					
				case "Q": 
					exit = true;
					break;
					
				default: 
					print("\nInvalid option\n");
					break;
				}
				
				Library.save();
			}			
		} catch (RuntimeException e) {
			print(e);
		}		
		print("\nEnded\n");
	}	

	
	private static void payfines() {
		new PayFineUI(new PayFineControl()).run();		
	}


	private static void listCurrentLoans() {
		print("");
		for (Loan loan : library.getCurrentLoans()) {
			print(loan + "\n");
		}		
	}



	private static void listBooks() {
		print("");
		for (Book book : library.getBooks()) {
			print(book + "\n");
		}		
	}



	private static void listMembers() {
		print("");
		for (Member member : library.getMembers()) {
			print(member + "\n");
		}		
	}



	private static void borrowBook() {
		new BorrowBookUI(new BorrowBookControl()).run();		
	}


	private static void returnBook() {
		new ReturnBookUI(new ReturnBookControl()).run();		
	}


	private static void fixBooks() {
		new FixBookUI(new FixBookControl()).run();		
	}


	private static void incrementDate() {
		try {
			int days = Integer.valueOf(getInput("Enter number of days: ")).intValue();
			calendar.incrementDate(days);
			library.checkCurrentLoans();
			print(dateFormatter.format(calendar.getDate()));
			
		} catch (NumberFormatException e) {
			 print("\nInvalid number of days\n");
		}
	}


	private static void addBook() {
		
		String author = getInput("Enter author: ");
		String title  = getInput("Enter title: ");
		String callNumber = getInput("Enter call number: ");
		Book book = library.addBook(author, title, callNumber);
		print("\n" + book + "\n");
		
	}

	
	private static void addMember() {
		try {
			String lastName = getInput("Enter last name: ");
			String firstName  = getInput("Enter first name: ");
			String email = getInput("Enter email address: ");
			int phoneNumber = Integer.valueOf(getInput("Enter phone number: ")).intValue();
			Member member = library.addMember(lastName, firstName, email, phoneNumber);
			print("\n" + member + "\n");
			
		} catch (NumberFormatException e) {
			 print("\nInvalid phone number\n");
		}
		
	}


	private static String getInput(String prompt) {
		System.out.print(prompt);
		return inputScanner.nextLine();
	}
	
	
	
	private static void print(Object object) {
		System.out.println(object);
	}

	
}
