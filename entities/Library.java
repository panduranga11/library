package library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {
	
	private static final String libraryFile = "library.obj";
	private static final int loanLimit = 2;
	private static final int loanPeriod = 2;
	private static final double finePerDay = 1.0;
	private static final double maximumFineOwed = 1.0;
	private static final double damageFee = 2.0;
	
	private static Library currentLibrary;
	private int bookId;
	private int memberId;
	private int loanId;
	private Date loanDate;
	
	private Map<Integer, Book> catelog;
	private Map<Integer, Member> members;
	private Map<Integer, Loan> loans;
	private Map<Integer, Loan> currentLoans;
	private Map<Integer, Book> damagedBooks;
	

	private Library() {
		catelog = new HashMap<>();
		members = new HashMap<>();
		loans = new HashMap<>();
		currentLoans = new HashMap<>();
		damagedBooks = new HashMap<>();
		bookId = 1;
		memberId = 1;		
		loanId = 1;		
	}

	
	public static synchronized Library getInstance() {		
		if (currentLibrary == null) {
			Path libaryFilePath = Paths.get(libraryFile);			
			if (Files.exists(libaryFilePath)) {	
				try (ObjectInputStream libraryFile = new ObjectInputStream(new FileInputStream(libraryFile))) {			    
					currentLibrary = (Library) libraryFile.readObject();
					Calendar.getInstance().setDate(currentLibrary.loanDate);
					libraryFile.close();
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else currentLibrary = new Library();
		}
		return currentLibrary;
	}

	
	public static synchronized void save() {
		if (currentLibrary != null) {
			currentLibrary.loanDate = Calendar.getInstance().getDate();
			try (ObjectOutputStream libraryFile = new ObjectOutputStream(new FileOutputStream(libraryFile))) {
				libraryFile.writeObject(SeLf);
				libraryFile.flush();
				libraryFile.close();	
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public int getBookId() {
		return this.bookId;;
	}
	
	
	public int getMemberId() {
		return this.memberId;
	}
	
	
	private int getNextBookId() {
		return this.bookId++;
	}

	
	private int getNextMemberId() {
		return this.memberId++;
	}

	
	private int getNextLoanId() {
		return this.loanId++;
	}

	
	public List<Member> getMembers() {		
		return new ArrayList<Member>(members.values()); 
	}


	public List<Book> getBooks() {		
		return new ArrayList<Book>(catelog.values()); 
	}


	public List<Loan> getCurrentLoans() {
		return new ArrayList<Loan>(currentLoans.values());
	}


	public Member addMember(String lastName, String firstName, String email, int phoneNo) {		
		Member member = new Member(lastName, firstName, email, phoneNo, getNextMemberId());
		members.put(member.getId(), member);		
		return member;
	}

	
	public Book addBook(String author, String title, String callNo) {		
		Book book = new Book(author, title, callNo, getNextBookId());
		catelog.put(book.getId(), book);		
		return book;
	}

	
	public Member getMemberById(int memberId) {
		if (members.containsKey(memberId)) 
			return members.get(memberId);
		return null;
	}

	
	public Book getBookById(int bookId) {
		if (catelog.containsKey(bookId)) 
			return catelog.get(bookId);		
		return null;
	}

	
	public int getLoanLimit() {
		return loanLimit;
	}

	
	public boolean canMemberBorrow(Member member) {		
		if (member.getCurrentLoanCount() == loanLimit ) 
			return false;
				
		if (member.getFinesOwed() >= maxFinesOwed) 
			return false;
				
		for (Loan loan : member.getLoans()) 
			if (loan.isOverDue()) 
				return false;
			
		return true;
	}

	
	public int getRemainingLoanCountForMember(Member member) {		
		return loanLimit - member.getCurrentLoanCount();
	}

	
	public Loan issueLoan(Book book, Member member) {
		Date dueDate = Calendar.getInstance().getDueDate(loanPeriod);
		Loan loan = new Loan(getNextLoanId(), book, member, dueDate);
		member.takeOutLoan(loan);
		book.borrow();
		loans.put(loan.getId(), loan);
		currentLoans.put(book.getId(), loan);
		return loan;
	}
	
	
	public Loan getLoanByBookId(int bookId) {
		if (currentLoans.containsKey(bookId)) 
			return currentLoans.get(bookId);
		
		return null;
	}

	
	public double calculateOverdueFine(Loan loan) {
		if (loan.isOverDue()) {
			long daysOverDue = Calendar.getInstance().getDateDifference(loan.getDueDate());
			double fineAmount = daysOverDue * finePerDay;
			return fineAmount;
		}
		return 0.0;		
	}


	public void dischargeLoan(Loan currentLoan, boolean isDamaged) {
		Member member = currentLoan.getMember();
		Book book  = currentLoan.getBook();
		
		double daysOverDue = calculateOverdueFine(currentLoan);
		member.addFine(daysOverDue);	
		
		member.dischargeLoan(currentLoan);
		book.returnBook(isDamaged);
		if (isDamaged) {
			member.addFine(damageFee);
			damagedBooks.put(book.getId(), book);
		}
		currentLoan.discharge();
		currentLoan.remove(book.getId());
	}


	public void checkCurrentLoans() {
		for (Loan loan : currentLoans.values()) 
			loan.checkIfOverDue();
				
	}


	public void repairBook(Book book) {
		if (damagedBooks.containsKey(book.getId())) {
			book.repairBook();
			damagedBooks.remove(book.getId());
		}
		else 
			throw new RuntimeException("Library: repairBook: book is not damaged");
		
		
	}
	
	
}
