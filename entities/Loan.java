package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
	
	public static enum LoanState { CURRENT, OVER_DUE, DISCHARGED };
	
	private int loanId;
	private Book book;
	private Member member;
	private Date dueDate;
	private LoanState loanState;

	
	public Loan(int loanId, Book book, Member member, Date dueDate) {
		this.loanId = loanId;
		this.book = book;
		this.member = member;
		this.dueDate = dueDate;
		this.loanState = LoanState.CURRENT;
	}

	
	public void checkIfOverDue() {
		if (loanState == LoanState.CURRENT &&
			Calendar.getInstance().getDate().after(dueDate)) 
			this.loanState = LoanState.OVER_DUE;			
		
	}

	
	public boolean isOverDue() {
		return loanState == LoanState.OVER_DUE;
	}

	
	public Integer getId() {
		return loanId;
	}


	public Date getDueDate() {
		return dueDate;
	}
	
	
	public String toString() {
		SimpleDateFormat dataFormat = new SimpleDateFormat("dd/MM/yyyy");

		StringBuilder loanInfoBuilder = new StringBuilder();
		loanInfoBuilder.append("Loan:  ").append(loanId).append("\n")
		  .append("  Borrower ").append(member.getId()).append(" : ")
		  .append(member.getLastName()).append(", ").append(member.getFirstName()).append("\n")
		  .append("  Book ").append(book.getId()).append(" : " )
		  .append(book.getTitle()).append("\n")
		  .append("  DueDate: ").append(dataFormat.format(dueDate)).append("\n")
		  .append("  State: ").append(loanState);		
		return loanInfoBuilder.toString();
	}


	public Member getMember() {
		return member;
	}


	public Book getBook() {
		return book;
	}


	public void discharge() {
		loanState = LoanState.DISCHARGED;		
	}

}
