package library.fixbook;
import java.util.Scanner;


public class FixBookUI {

	public static enum UIState { INITIALISED, READY, FIXING, COMPLETED };

	private FixBookControl fixBookControl;
	private Scanner inputScanner;
	private UIState uIState;

	
	public FixBookUI(FixBookControl fixBookControl) {
		this.fixBookControl = fixBookControl;
		inputScanner = new Scanner(System.in);
		uIState = UIState.INITIALISED;
		fixBookControl.setUI(this);
	}


	public void setState(UIState uIState) {
		this.uIState = uIState;
	}

	
	public void run() {
		print("Fix Book Use Case UI\n");
		
		while (true) {
			
			switch (uIState) {
			
			case READY:
				String bookIdString = getInput("Scan Book (<enter> completes): ");
				if (bookIdString.length() == 0) 
					fixBookControl.scanningComplete();				
				else {
					try {
						int bookId = Integer.valueOf(bookIdString).intValue();
						fixBookControl.bookScanned(bookId);
					}
					catch (NumberFormatException e) {
						print("Invalid bookId");
					}
				}
				break;	
				
			case FIXING:
				String answer = getInput("Fix Book? (Y/N) : ");
				boolean shouldFix = false;
				if (answer.toUpperCase().equals("Y")) 
					shouldFix = true;				
				fixBookControl.fixBook(shouldFix);
				break;
								
			case COMPLETED:
				print("Fixing process complete");
				return;
			
			default:
				print("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + uIState);			
			
			}		
		}
		
	}

	
	private String getInput(String prompt) {
		System.out.print(prompt);
		return inputScanner.nextLine();
	}			
		
	private void print(Object object) {
		System.out.println(object);
	}	

	public void display(Object object) {
		print(object);
	}
	
	
}
