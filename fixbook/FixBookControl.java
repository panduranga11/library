package library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class FixBookControl {
	
	private FixBookUI fixBookUI;
	private enum ControlState { INITIALISED, READY, FIXING };
	private ControlState controlState;
	
	private Library library;
	private Book currentBook;


	public FixBookControl() {
		this.library = Library.getInstance();
		controlState = controlState.INITIALISED;
	}
	
	
	public void setUI(FixBookUI fixBookUI) {
		if (!controlState.equals(controlState.INITIALISED)) 
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.fixBookUI = fixBookUI;
		fixBookUI.setState(FixBookUI.UIState.READY);
		controlState = controlState.READY;		
	}


	public void bookScanned(int bookId) {
		if (!controlState.equals(controlState.READY)) 
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		currentBook = library.getBookById(bookId);
		
		if (currentBook == null) {
			fixBookUI.display("Invalid bookId");
			return;
		}
		if (!currentBook.isDamaged()) {
			fixBookUI.display("Book has not been damaged");
			return;
		}
		fixBookUI.display(currentBook.toString());
		fixBookUI.setState(FixBookUI.UIState.FIXING);
		controlState = controlState.FIXING;		
	}


	public void fixBook(boolean shouldFix) {
		if (!controlState.equals(controlState.FIXING)) 
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (shouldFix) 
			library.repairBook(currentBook);
		
		currentBook = null;
		fixBookUI.setState(FixBookUI.UIState.READY);
		controlState = controlState.READY;		
	}

	
	public void scanningComplete() {
		if (!controlState.equals(controlState.READY)) 
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		fixBookUI.setState(FixBookUI.UIState.COMPLETED);		
	}

}
